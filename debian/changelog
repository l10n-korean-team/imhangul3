imhangul (3.1.0-3) UNRELEASED; urgency=low

  [ Changwoo Ryu ]
  * Use the copyright format 1.0
  * Don't suggests imhangul-status-applet which is not available anymore

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 09 Jun 2012 22:12:50 +0900

imhangul3 (3.1.0-2) unstable; urgency=low

  [ Changwoo Ryu ]
  * debian/watch
    - Rewrite it without using the googlecode redirector.
  * Use debhelper 9
  * Standards-Version: 3.9.3
  * Multiarch transition

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 20 May 2012 03:35:12 +0900

imhangul3 (3.1.0-1) unstable; urgency=low

  [ Changwoo Ryu ]
  * New upstream release
  * debian/watch:
    - Use the new upstream site in Google Code

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 31 Dec 2011 20:47:46 +0900

imhangul3 (3.0.0-1) unstable; urgency=low

  [ Changwoo Ryu ]
  * New upstream release
  * debian/watch
    - Watch 3.x versions only
  * Standards-Version: 3.9.2

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 22 May 2011 22:05:06 +0900

imhangul3 (0.9.16-1+gtk3+4) experimental; urgency=low

  [ Changwoo Ryu ]
  * Correct git paths

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 25 Mar 2011 03:33:56 +0900

imhangul3 (0.9.16-1+gtk3+3) experimental; urgency=low

  [ Changwoo Ryu ]
  * New source package name to coexist with the gtk2 imhangul.
  * Updated the description.

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 25 Mar 2011 01:16:14 +0900

imhangul (0.9.16-1+gtk3+2) experimental; urgency=low

  * Team maintained by Debian Korean L10N
  * Build-Depends on dh-autoreconf

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 22 Mar 2011 00:32:46 +0900

imhangul (0.9.16-1+gtk3+1) experimental; urgency=low

  * Experimental GTK3 immodule.
  * Rewrite debian/copyright in the DEP-5 format

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 20 Mar 2011 23:32:09 +0900

imhangul (0.9.16-1) unstable; urgency=low

  * New upstream release
  * Fix debian/watch for the new KLDP.net site
  * Standards-Version: 3.9.1
  * Use source format 3.0 (quilt)
  * Switch to debhelper7 from CDBS
  * im-config support

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 09 Jan 2011 03:09:32 +0900

imhangul (0.9.15-1) unstable; urgency=low

  * New upstream release
  * Standards-Version: 3.8.3
  * Build-Depends on the newer libhangul-dev 0.0.10

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 01 Nov 2009 00:38:42 +0900

imhangul (0.9.14-2) unstable; urgency=low

  * Correct the backspace problem on gtkhtml, with fix from the upstream.
    http://kldp.net/tracker/index.php?aid=305211
  * Clean up -- use configure flag --with-gtk-im-module-dir instead of
    directly overriding make variable.

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 13 Jan 2009 13:24:43 +0900

imhangul (0.9.14-1) unstable; urgency=low

  * New upstream release
  * Standards-Version: 3.8.0.
  * Build-Depends on libhangul-dev.
  * Correct typos and clean up the Debian specific docs. (README.Debian,
    README.Debian.ko, copyright)
  * Add the configuration example file.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 22 Dec 2008 14:28:46 +0900

imhangul (0.9.13-8) unstable; urgency=low

  * Added Vcs-Browser and Vcs-Git fields.
  * Removed the beginning "A" article from the short description,
    according to Developer Reference 6.2.2.
  * Removed the duplicated homepage URL from the description.
  * Removed empty postrm script.
  * Corrected copyright so lintian understand it.
  * Removed unnecessary debian revision from libgtk2.0-dev Build-Depends.
  * Switched to cdbs and simple-patchsys.

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 03 May 2008 16:20:27 +0900

imhangul (0.9.13-7) unstable; urgency=low

  * Standard-Version: 3.7.3.
  * Removed /usr/lib/gtk-2.0/*/immodules/*.la file which is not used for 
    a GTK+ immodule.
  * Added watch file.
  * Don't ignore "make distclean" error.
  * Removed Build-Depends on bzip2 which is not used anymore.
  * Fixed the homepage URL in the description, copyright, and README.Debian*.
  * Added Homepage: field to debian/control.

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 18 Jan 2008 16:37:41 +0900

imhangul (0.9.13-6) unstable; urgency=low

  * Used pkg-config gtk_binary_version variable instead of modversion, to
    find the immodule path.  (Closes:419365)

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 16 Apr 2007 00:05:43 +0900

imhangul (0.9.13-5) unstable; urgency=low

  * Build for unstable gtk 2.10. (Closes: #419309)
  * Depends on debhelper 5.

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 15 Apr 2007 18:04:36 +0900

imhangul (0.9.13-4) experimental; urgency=low

  * Bumped Standard-Version to 3.7.2.
  * used dh_gtkmodules instead of update-gtk-immodules.
  * Detect the immodule path in build time.  No more debian/rules
    modification on every new stable GTK+ release...

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 16 Mar 2007 04:00:02 +0900

imhangul (0.9.13-3) unstable; urgency=low

  * Set GTK_IM_MODULE=hangul2 instead of GTK_IM_MODULE=imhangul.  For
    hangul3{f,90} users, no idea...

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 25 Dec 2005 23:30:50 +0900

imhangul (0.9.13-2) unstable; urgency=low

  * Used im-switch.

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 23 Dec 2005 03:48:06 +0900

imhangul (0.9.13-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 10 Oct 2005 15:06:16 +0900

imhangul (0.9.12-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sun,  7 Aug 2005 15:39:49 +0900

imhangul (0.9.11-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 11 Aug 2004 20:06:25 +0900

imhangul (0.9.10-3) unstable; urgency=low

  * Disable the status window to avoid Epiphany crashes.

 -- Changwoo Ryu <cwryu@debian.org>  Sun,  8 Aug 2004 13:33:12 +0900

imhangul (0.9.10-2) unstable; urgency=low

  * Set the default preedit style as "underline".

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 23 Jul 2004 05:46:28 +0900

imhangul (0.9.10-1) unstable; urgency=low

  * New upstream release.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 19 Jul 2004 01:02:04 +0900

imhangul (0.9.9-6) unstable; urgency=low

  * Removed the note about gtk's broken hangul module from the
    Description:.  The broken module has been removed in gtk 2.4.x.
  * Changed the module install dir to /usr/lib/gtk-2.0/2.4.0/immodules,
    from /usr/lib/gtk-2.0/2.4.0/immodules/hangul.
  * Corrected some old informations in README.Debian.  More information to
    set the default GTK input module.

 -- Changwoo Ryu <cwryu@debian.org>  Thu,  8 Jul 2004 02:53:56 +0900

imhangul (0.9.9-5) unstable; urgency=low

  * DebConf4 release.  ;)
  * Rebuilt for unstable.

 -- Changwoo Ryu <cwryu@debian.org>  Tue,  1 Jun 2004 08:11:23 +0900

imhangul (0.9.9-4) experimental; urgency=low

  * Depends: libgtk2.0-dev (>= 2.4.0) for experimental.

 -- Changwoo Ryu <cwryu@debian.org>  Thu, 15 Apr 2004 01:46:49 +0900

imhangul (0.9.9-3) experimental; urgency=low

  * GTK 2.4 build.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 29 Mar 2004 22:13:50 +0900

imhangul (0.9.9-2) unstable; urgency=low

  * Changed the Section to gnome
  * Simplified postinst, postrm scripts.

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 13 Jan 2004 05:35:01 +0900

imhangul (0.9.9-1) unstable; urgency=low

  * New upstream release.  It obsoltes the patch in 0.9.8-2.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 22 Dec 2003 23:38:07 +0900

imhangul (0.9.8-2) unstable; urgency=low

  * Fixes "Evolution Never Die" bug.

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 10 Dec 2003 04:02:44 +0900

imhangul (0.9.8-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 23 Nov 2003 03:47:26 +0900

imhangul (0.9.7-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 22 Oct 2003 02:56:43 +0900

imhangul (0.9.6-2) unstable; urgency=low

  * Added imhangul-status-applet to Suggests:.
  * Added bzip2 to Build-Depends:.

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 26 Apr 2003 15:03:53 +0900

imhangul (0.9.6-1) unstable; urgency=low

  * Initial Release (Closes: #190360).

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 23 Apr 2003 22:15:08 +0900

